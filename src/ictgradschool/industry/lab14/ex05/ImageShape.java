package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;
    ImageShapeWorker worker;

    public class ImageShapeWorker extends SwingWorker<Image, Void>{

        int width;
        int height;
        URL url;

        public ImageShapeWorker(int width, int height, URL url) {
            this.width = width;
            this.height = height;
            this.url = url;
        }

        @Override
        protected void done() {
            try {
                image = get();
            } catch (InterruptedException e){
                e.getMessage();
            } catch (ExecutionException e){
                e.getMessage();
            }
        }

        @Override
        protected Image doInBackground() throws Exception {
            try {
                Image image = ImageIO.read(url);
                if (width == image.getWidth(null) && height == image.getHeight(null)) {
                    image = image;
                } else {
                    image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    image.getHeight(null);
                }
                return image;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        worker = new ImageShapeWorker(width,height,url);
        worker.execute();
    }


    @Override
    public void paint(Painter painter) {

        if (!worker.isDone()){
            painter.setColor(Color.BLUE);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.green);
            painter.drawCenteredText("Loading, gone fishing", fX, fY, fWidth, fHeight);

            System.out.println("loading ");
        } else {

            painter.drawImage(this.image, fX, fY, fWidth, fHeight);

        }
    }
}
