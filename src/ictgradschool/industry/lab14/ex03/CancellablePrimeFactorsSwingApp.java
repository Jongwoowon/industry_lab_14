package ictgradschool.industry.lab14.ex03;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class CancellablePrimeFactorsSwingApp extends JPanel {

    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _abortBtn;
    private JTextArea _factorValues;  // Component to display the result.

    public class PrimeFactorisationWorker extends SwingWorker<ArrayList<Long>, Void> {

        protected long n;

        public PrimeFactorisationWorker(long n) {
            this.n = n;
        }

        @Override
        protected ArrayList<Long> doInBackground() throws Exception {

            ArrayList<Long> nlist = new ArrayList<>();
            if (isCancelled()){
                return null;
            } else {
                // Start the computation in the Event Dispatch thread.
                for (long i = 2; i * i <= n; i++) {

                    // If i is a factor of N, repeatedly divide it out
                    while (n % i == 0) {
                        nlist.add(i);
                        n = n / i;
                    }
                }

                // if biggest factor occurs only once, n > 1
                if (n > 1) {
                    nlist.add(n);
                }
                return nlist;
            }
        }

        @Override
        protected void done() {
            ArrayList<Long> nlist;
            nlist = null;

            // Re-enable the Start button.
            _startBtn.setEnabled(true);

            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());

            try {
                if(!isCancelled()){
                    nlist = get();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            for (Long n : nlist) {
                _factorValues.append(n + "\n");
            }
        }
    }

    PrimeFactorisationWorker primeW;
    long n;

    public CancellablePrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);


        _startBtn = new JButton("Compute");
        _abortBtn = new JButton("Abort!");
        _abortBtn.setEnabled(false);
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        //create new cancel button


        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known. // enable Abort Button to be clicked
                _startBtn.setEnabled(false);
                _abortBtn.setEnabled(true);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                primeW = new PrimeFactorisationWorker(n);
                primeW.execute();
            }
        });

        _abortBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                // aborts the worker when pressed
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                _abortBtn.setEnabled(true);
                _startBtn.setEnabled(true);
                _factorValues.setText(null);
                tfN.setText(null);

                try {
                    primeW.cancel(true);
                } catch (NullPointerException e){
                    e.getMessage();
                }

            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new CancellablePrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

